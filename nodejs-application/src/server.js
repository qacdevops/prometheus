'use strict';

const express = require('express');

const PORT = 5000;
const HOST = '0.0.0.0';

const client = require('prom-client');
const collectDefaultMetrics = client.collectDefaultMetrics;
collectDefaultMetrics({ timeout: 5000 });

const counter = new client.Counter({
  name: 'node_request_operations_total',
  help: 'The total number of processed requests'
});

const histogram = new client.Histogram({
  name: 'node_request_duration_seconds',
  help: 'Histogram for the duration in seconds.',
  buckets: [1, 2, 5, 6, 10]
});

// const help_counter = new client.Counter({
//   name: 'node_help_request_operations_total',
//   help: 'The total number of processed requests on the Help Page'
// });

const app = express();
app.get('/', (req, res) => {

  var start = new Date()
  var simulateTime = 1000

  setTimeout(function(argument) {
    var end = new Date() - start
    histogram.observe(end / 1000);
  }, simulateTime)

  counter.inc();
  
  res.send('NodeJS App is up and running!\n');
});


app.get('/metrics', (req, res) => {
  res.set('Content-Type', client.register.contentType)
  res.end(client.register.metrics())
})

// app.get('/help', (req, res) =>{
//   help_counter.inc()
//   res.send('This is the help page!')
// })

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
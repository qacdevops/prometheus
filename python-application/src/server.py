from flask import Response, Flask, request
from prometheus_client import Summary, Counter, Histogram, Gauge, generate_latest
from prometheus_client.core import CollectorRegistry
import time

app = Flask(__name__)

graphs = {}
graphs['count_root'] = Counter('python_request_operations_total', 'The total number of processed requests')
graphs['hist_root'] = Histogram('python_request_duration_seconds', 'Histogram for the duration in seconds.', buckets=(1, 2, 5, 6, 10))
# graphs['count_help'] = Counter('python_request_help_total', 'The total number of processed requests for help route')

@app.route("/")
def hello():
    start = time.time()
    graphs['count_root'].inc()
    
    time.sleep(0.500)
    end = time.time()
    graphs['hist_root'].observe(end - start)
    return "Flask App is up and running!"

@app.route("/metrics")
def requests_count():
    res = []
    for item,sub_item in graphs.items():
        res.append(generate_latest(sub_item))
    return Response(res, mimetype="text/plain")

# @app.route("/help")
# def help_count():
#     graphs['count_help'].inc()
#     return "This is the help page!"
# Prometheus Example

Pre-Requisites:

- Docker installed:
```bash
curl https://get.docker.com | sudo bash  
sudo usermod -aG docker $USER
```
- Docker-Compose installed:
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose  
sudo chmod +x /usr/local/bin/docker-compose
```
- Ports `5000`, `8000`, `3000` and `9090` open

To run the Application:

```bash
git clone https://gitlab.com/qacdevops/prometheus.git && cd prometheus
docker-compose up -d --build
```

Once the application is up and running: 

- The Flask app is on `port 5000`
- The NodeJS app is on `port 8000`
- Grafana is on `port 3000`
- Prometheus is on `port 9090`
